import pandas as pd


import pandas as pd

def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()
    output = []
    def extract_title(name):
        title = name.split(',')[1].split('.')[0].strip()
        return title

    df['Title'] = df['Name'].apply(extract_title)

    titles = ["Mr.", "Mrs.", "Miss."]
    output = []
    for title in titles:
        title_df = df[df['Title'] == title]
        missing = title_df['Age'].isnull().sum()
        median_age = title_df['Age'].median()
        median_value = int(round(median_age)) if pd.notna(median_age) else None
        
        output.append((title, missing, median_value))
    return output
